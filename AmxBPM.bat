REM This batch file can be used to provision an AMXBPM docker test enivronment on Azure.
Rem usage: AmxBPM <Azure Service Principal> <Azure password> <Azure Tenant> <Azure Resource Group Name>
REM The Resource Group may NOT exist. It is newly created. The AMX BPM docker VM is provisioned in this group 

call az login --service-principal -u %1 --password %2 --tenant %3
call az group create --name %4 --location westeurope
call az group deployment create --name AMXBPMTestDeployment --resource-group %4 --template-uri https://gitlab.com/jbrandsma/AABCloudAMXBPMDev/raw/master/template.json --parameters @parameters.json