#!/bin/bash
# This script is used to load the TIBCO AMX BPM Image in docker
# Usage loadBPMDockerImage <Docker Registery Host Name> <UserID for Docker Registrery> <Password of Docker Registry> <docker image name> <docker image version>
docker login $1 -u $2 -p $3
docker pull $4:$5
docker run -d -p 8120:8120 -p 8080:8080 -p 10801:10801 -p 7222:7222 -p 19767:19767 -h localhost  --name=bpmcontainer testregistry99.azurecr.io/tibco/bpm:version1.0